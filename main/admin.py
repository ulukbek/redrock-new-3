from django.contrib import admin

# Register your models here.
from main.models import *

admin.site.register(MainSlider),
admin.site.register(MainInformation),
admin.site.register(Media),
admin.site.register(TravelersPhotos)
