# coding=utf-8
import threading

from django.core.exceptions import ObjectDoesNotExist
from django.db.models import Q
from django.core.mail import EmailMessage
from django.http import JsonResponse

from blog.forms import SubscribeForms
from .forms import SubmitForm
from django.shortcuts import render
from django.template import loader
from blog.models import NewsAndBlog, Subscribers
from .models import TravelersPhotos
from tours.models import Tour
from things_to_do.models import ThingsToDo
from main.models import MainSlider, MainInformation


def generate_view_params(request):
    form = SubscribeForms(request.POST)
    params = {
        'feedback_form': SubmitForm(request.POST),
        'form': form
    }
    return params


def subscriber_create(request):
    form = SubscribeForms(request.POST)
    context = {
        'form': form,
    }
    if request.POST:
        if form.is_valid():
            try:
                email = Subscribers.objects.get(email=form.cleaned_data['email'])
                return JsonResponse(dict(success=False, message='This email already exists'))
            except ObjectDoesNotExist:
                context.update(
                    dict(posted=True, email=form.cleaned_data['email']))
                form.save()
                return JsonResponse(dict(success=True, message='Successfully'))
    else:
        return JsonResponse(dict(success=False, message='Here is an error'))
    return JsonResponse(dict(success=False, message='Here is an error'))


def subscriber_delete(request):
    form = SubscribeForms(request.POST)
    context = {
        'form': form,
    }
    if request.POST:
        if form.is_valid():
            try:
                email = Subscribers.objects.get(email=form.cleaned_data['email'])
                email.delete()
                return JsonResponse(
                    dict(success=False, message='This email correctly deleted from our database for newsletter'))
            except ObjectDoesNotExist:
                return JsonResponse(dict(success=True, message='Your email does not exists in our newsletter database'))
    else:
        return JsonResponse(dict(success=False, message='Lol kek'))
    return JsonResponse(dict(success=False, message='Here is an error'))


def subscriber_delete_page(request):
    context = {

    }
    context.update(generate_view_params(request))

    return render(request, 'subscriber_delete.html', context)


def index(request):
    popular_tours = Tour.objects.filter(Q(popular_tour=True))
    things_to_do = ThingsToDo.objects.all().order_by('-id')[:10]
    context = {
        'slider': MainSlider.objects.all(),
        'info': MainInformation.objects.first(),
        'things': things_to_do,
        'travelers_photo': TravelersPhotos.objects.all(),
        'blog': NewsAndBlog.objects.all(),
        'popular': popular_tours,
    }

    context.update(generate_view_params(request))
    return render(request, 'index.html', context)


def feedback(request):
    form = SubmitForm(request.POST)
    if request.POST:
        if form.is_valid():
            t = loader.get_template('partial/_feedback_email.html')
            c = dict(phone=form.cleaned_data['phone'], email=form.cleaned_data['email'], text=form.cleaned_data['text'])
            letter = t.render(c, request)

            thread = threading.Thread(target=send_notification_email,
                                      args=('Новое сообщение', letter, 'wowtsty@gmail.com'))
            thread.start()

            return JsonResponse(dict(success=True, message='Lol Kek'))
        else:
            return JsonResponse(dict(success=False, message='Lol Kek'))
    return JsonResponse(dict(success=False, message='Lol Kek')),


def send_notification_email(title, body, to):
    email = EmailMessage(title, body=body, to=[to])
    email.content_subtype = 'html'
    email.send()
