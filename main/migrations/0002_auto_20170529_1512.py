# -*- coding: utf-8 -*-
# Generated by Django 1.10 on 2017-05-29 09:12
from __future__ import unicode_literals

import ckeditor.fields
from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='maininformation',
            name='text',
            field=ckeditor.fields.RichTextField(max_length=1000, verbose_name='\u041a\u0440\u0430\u0442\u043a\u0430\u044f \u0438\u043d\u0444\u043e\u0440\u043c\u0430\u0446\u0438\u044f'),
        ),
    ]
