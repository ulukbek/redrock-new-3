from __future__ import unicode_literals

from django.apps import AppConfig


class ThingsToDoConfig(AppConfig):
    name = 'things_to_do'
