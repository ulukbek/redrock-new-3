/**
 * Created by Nurs on 15.04.2017.
 */
$(document).ready(function () {
    $('.clients_slider').slick({
        nextArrow: '<button type="button" class="next_client"></button>',
        prevArrow: '<button type="button" class="prev_client"></button>',
        speed: 1000,
        adaptiveHeight: true,
        responsive: [
            {
                breakpoint: 768,
                settings: {
                    arrows: false,
                }
            },
            {
                breakpoint: 480,
                settings: {
                    arrows: false,
                }
            }
        ]

    });
});