/**
 * Created by Nurs on 15.04.2017.
 */
$(document).ready(function () {
    $('.get_trip label').on('click', function () {
        $(this).addClass('label_active');
        $($(this).parent().siblings().find('label').removeClass('label_active'));
        $($(this).parent().siblings().find('input').attr('checked', false));
        $($(this).parent().find('input')).attr('checked', true);
        $($(this).parent().find('input')).removeAttr('disabled');
        $($(this).parent().siblings().find('input')).attr('disabled', 'disabled');

    });
    $('.hotels_slider').each(function (i, obj) {
        $(obj).slick({
            slidesToShow: 4,
            slidesToScroll: 1,
            nextArrow: '<button type="button" class="next_hotel"></button>',
            prevArrow: '<button type="button" class="prev_hotel"></button>',
            centerMode: false,
            infinite: false,
            responsive: [
                {
                    breakpoint: 768,
                    settings: {
                        arrows: false,
                        slidesToShow: 3
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        arrows: false,
                        slidesToShow: 1
                    }
                }
            ]
        });
    });
    $('.choose_this_hotel').on('click', function () {
        $(this).addClass('selected_hotel');
        $($(this).parent().css({
            'transform': 'translateY(' + 0 + '%' + ')'
        }));
        $($(this).parent().parent().parent().siblings().find('.hotel_image').find('.choose_hotel_effect').css({
            'transform': 'translateY(' + -100 + '%' + ')'
        }));
        $($(this).parent().find('input').attr('checked', true));
        $($(this).parent().parent().parent().siblings().find('.hotel_image').find('.choose_hotel_effect').find('input').attr('checked', false));
        $($(this).parent().parent().parent().siblings().find('.hotel_image').find('.choose_hotel_effect').find('.choose_this_hotel').removeClass('selected_hotel'));
    });
    $('.trip_fill_form label').on('click', function () {
        $('.choose_hotel_effect input').attr('checked', false);
        $('.choose_hotel_effect .choose_this_hotel').removeClass('selected_hotel');
        $('.choose_hotel_effect').css({
            'transform': 'translateY(' + -100 + '%' + ')',
        });
        $('.transport_block input').attr('checked', false);
        $('.transport_block .transport_type').removeClass('selected_transport');
        $('.food_block input').attr('checked', false);
        $('.food_block .food_type label').removeClass('label_active')
        var block = $(this).attr('data-link').replace('#', '');
        var get_block = document.getElementById(block);
        $(get_block).siblings().fadeOut("slow");
        $(get_block).fadeIn("slow", function () {
            $(get_block).find('.hotels_slider').slick('slickNext');
        });
    });
    $('.transport_type').on('click', function () {
        $('input', this).attr('checked', true);
        $(this).addClass('selected_transport');
        $(this).siblings().find('input').attr('checked', false);
        $(this).siblings().removeClass('selected_transport');
    })
});