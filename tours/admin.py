from django.contrib import admin
from jet.admin import CompactInline

from .models import *


# Register your models here.


class DaysInline(admin.TabularInline):
    model = Days
    extra = 1


class TourAdmin(admin.ModelAdmin):
    inlines = (DaysInline,)


admin.site.register(Tour, TourAdmin)
admin.site.register(Category)
admin.site.register(TourDetails)
